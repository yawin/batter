/*
  DOUGH ENGINE
  Copyright (C) 2020 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "scenemanager.h"
using namespace RF_Structs;

SceneManager* SceneManager::instance = nullptr;

SceneManager::SceneManager()
{
  if(instance == nullptr)
  {
    instance = this;
  }
  else
  {
    delete this;
  }
}

SceneManager::~SceneManager()
{
  instance = nullptr;
}

bool SceneManager::isInList(std::string key)
{
  return (Get(key) != nullptr);
}

void SceneManager::Start(std::string key)
{
  if(!isInList(key)){return;}

  if(SceneManager::instance->sceneStack.size() > 0)
  {
    RF_Engine::sendSignal(SceneManager::instance->sceneStack.top()->id, S_SLEEP_TREE);
  }

  SceneManager::instance->sceneStack.push(Get(key));
  SceneManager::instance->sceneStack.top()->Init();
}

void SceneManager::Stop()
{
  if(instance != nullptr)
  {
    SceneManager::instance->sceneStack.top()->Stop();
    SceneManager::instance->sceneStack.pop();

    if(SceneManager::instance->sceneStack.size() > 0)
    {
      RF_Engine::sendSignal(SceneManager::instance->sceneStack.top()->id, S_AWAKE_TREE);
    }
  }
}
void SceneManager::Change(std::string key)
{
  if(!isInList(key)){return;}
  SceneManager::Stop();
  SceneManager::Start(key);
}

std::string SceneManager::activeSceneId()
{
  if(SceneManager::instance != nullptr)
  {
    if(SceneManager::instance->sceneStack.size() > 0)
    {
      return SceneManager::instance->sceneStack.top()->sceneid;
    }
  }

  return "";
}
