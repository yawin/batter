/*
  DOUGH ENGINE
  Copyright (C) 2020 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "inputmanager.h"

bool InputManager::False = false;
std::map<std::string, std::pair<KeyGroup*, KeyGroup*>> InputManager::Axis;

void InputManager::setAxis(std::string name, KeyGroup* positive, KeyGroup* negative)
{
  Axis[name] = std::make_pair(positive, negative);
}

int InputManager::getAxis(std::string name)
{
  int ret = 0;

  std::map<std::string, std::pair<KeyGroup*, KeyGroup*>>::const_iterator it = Axis.find(name);
  if(it != Axis.end())
  {
    if(Axis[name].first->Get()){ ret++; }
    if(Axis[name].second->Get()){ ret--; }
  }

  return ret;
}

void InputManager::clearAxis(std::string name)
{
  if(name == "")
  {
    Axis.clear();
  }
  else
  {
    Axis.erase(name);
  }
}

KeyGroup::KeyGroup(std::vector<bool*> _keys)
{
  for(bool* b : _keys)
  {
    keys.push_back(b);
  }
}

KeyGroup::~KeyGroup()
{
  keys.clear();
}

bool KeyGroup::Get()
{
  bool ret = false;
  for(bool* b : keys)
  {
    ret = ret | *b;
  }

  return ret;
}


KeyGroup* KeyGroup::Make(std::vector<bool*> keys)
{
  return new KeyGroup(keys);
}
