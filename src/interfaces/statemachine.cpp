#include "interfaces/statemachine.h"

void StateMachine::addState(std::string id, std::function<void()> func, std::string next)
{
  if(activeState == ""){activeState = id;}
  stateList.emplace(id, State{id, func, next});
}

void StateMachine::doAction()
{
  if(stateList[activeState].action != nullptr)
  {
    stateList[activeState].action();

    if(stateList[activeState].next != "")
    {
      activeState = stateList[activeState].next;
    }
  }
}

void StateMachine::setState(std::string state)
{
  if(stateList.count(state) != 0)
  {
    activeState = state;
  }
}

std::string StateMachine::getState()
{
  return activeState;
}
