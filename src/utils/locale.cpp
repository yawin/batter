#include <RosquilleraReforged/utils/parser.h>
#include "utils/locale.h"

namespace Locale
{
  static string lang = "";

  void setLang(string language)
  {
    lang = language;
  }

  string getLang()
  {
    return lang;
  }

  string getText(string key)
  {
    if(lang == "")
    {
      return "Language not configured";
    }

    Parser parser("locale/" + lang + ".json");
    string tmp = "";
    if(parser.loaded)
    {
      Parser::json_string(parser.jsvalue[key], tmp);
    }
    return tmp;
  }
}
