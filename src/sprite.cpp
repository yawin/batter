/*
  DOUGH ENGINE
  Copyright (C) 2020 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "sprite.h"
#include "camera.h"
using namespace RF_Structs;

Sprite::Sprite(string name):RF_Process(name)
{
  animator = new Animator(&graph);
}

Sprite::~Sprite()
{
  delete animator;
}

void Sprite::Draw()
{
  animator->Draw();

  if(graph != nullptr)
  {
    if(updateDepth){ zLayer = realPosition.y;}
  }

  OnSpriteDraw();
}

void Sprite::LateDraw()
{
  if(Camera::instance != nullptr)
  {
    offset.x = (transform.scale.x * graph->w * 0.5);
    offset.y = (transform.scale.y * graph->h * 0.5);

    transform.scale.x = Camera::DrawModifier().scale.x / graph->w;
    transform.scale.y = Camera::DrawModifier().scale.y / graph->h;

    transform.position.x = (int)(Camera::DrawModifier().position.x) + realPosition.x * Camera::DrawModifier().scale.x + offset.x;
    transform.position.y = (int)(Camera::DrawModifier().position.y) + realPosition.y * Camera::DrawModifier().scale.y + offset.y;
  }

  OnSpriteLateDraw();
}

void Sprite::SetPosition(float x, float y)
{
  realPosition.x = x;
  if(realPosition.x < 0) realPosition.x = 0;
  else if(realPosition.x >= Camera::RoomSize().x) realPosition.x = Camera::RoomSize().x - 1;

  realPosition.y = y;
  if(realPosition.y < 0) realPosition.y = 0;
  else if(realPosition.y >= Camera::RoomSize().y) realPosition.y = Camera::RoomSize().y - 1;
}

void Sprite::Move(float x, float y)
{
  realPosition.x += x;
  if(!canExitRoom && realPosition.x < 0) realPosition.x = 0;
  else if(!canExitRoom && realPosition.x >= Camera::RoomSize().x) realPosition.x = Camera::RoomSize().x - 1;

  realPosition.y += y;
  if(!canExitRoom && realPosition.y < 0) realPosition.y = 0;
  else if(!canExitRoom && realPosition.y >= Camera::RoomSize().y) realPosition.y = Camera::RoomSize().y - 1;
}
