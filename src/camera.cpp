/*
  DOUGH ENGINE
  Copyright (C) 2020 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "camera.h"

#include <RosquilleraReforged/rf_engine.h>
using namespace RF_Structs;

Camera* Camera::instance = nullptr;

Camera::Camera():RF_Process("Camera")
{
  if(instance == nullptr)
  {
    instance = this;
  }
  else
  {
    delete this;
    return;
  }

  windowLimit.x = RF_Engine::MainWindow()->width();
  windowLimit.y = RF_Engine::MainWindow()->height();
}

Camera::~Camera()
{
  instance = nullptr;
}

Vector2<float> Camera::RoomSize()
{
  return instance->roomSize;
}

const RF_Structs::Transform2D<float, float, float>& Camera::DrawModifier()
{
  return instance->transform;
}

void Camera::ProcessRoomSize()
{
  float w = windowLimit.x / roomSize.x;
  float h = windowLimit.y / roomSize.y;

  transform.scale.x = (int) ((w < h) ? w : h);
  transform.scale.y = transform.scale.x;

  transform.position.x = (windowLimit.x - (transform.scale.x * roomSize.x)) * 0.5;
  transform.position.y = (windowLimit.y - (transform.scale.y * roomSize.y)) * 0.5;
}
