/*
  DOUGH ENGINE
  Copyright (C) 2020 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#include "animator.h"

#include <RosquilleraReforged/rf_assetmanager.h>
#include <RosquilleraReforged/rf_engine.h>
#include <assert.h>

Animator::Animator(SDL_Surface **g)
{
  graph = g;
  selectedAnimation = "";
}
Animator::~Animator()
{
  animationList.clear();
}

void Animator::setGraph(SDL_Surface **g)
{
  graph = g;
}

void Animator::Add(std::string aID, const char *b, int f, float s, bool l)
{
  Add(aID, "", b, f, s, l);
}

void Animator::Add(std::string aID, const char *p, const char *b, int f, float s, bool l)
{
  assert(!isInList(aID));
  animationList[aID] = new Animation(p, b, f, s, l);

  if(selectedAnimation == "")
  {
    tryToExecute(aID);
  }
}

void Animator::Clear()
{
  selectedAnimation = "";
  animationList.clear();
}

bool Animator::isInList(std::string key)
{
  return (Get(key) != nullptr);
}

Animation* Animator::Get(std::string key)
{
  return animationList[key];
}

void Animator::tryToExecute(std::string state)
{
  assert(isInList(state));
  assert(RF_AssetManager::isLoaded(animationList[state]->Package(), animationList[state]->Basename() + "_0"));

  if(selectedAnimation != state)
  {
    selectedAnimation = state;
    actualAnimation = animationList[selectedAnimation];
    actualFrame = 0.0;
  }
}

void Animator::Draw()
{
  if(actualAnimation != nullptr && selectedAnimation != "")
  {
    animating = true;
    actualFrame += (actualAnimation->Speed()*RF_Engine::instance->Clock.deltaTime);

    if(actualFrame < 0.0)
    {
      animating = actualAnimation->IsLoop();
      actualFrame = (actualAnimation->IsLoop()) ? 0.9 + (float)actualAnimation->FrameAmount()-1 : 0.0;
    }
    else if(actualFrame > 0.9 + (float)actualAnimation->FrameAmount()-1)
    {
      animating = actualAnimation->IsLoop();
      actualFrame = (actualAnimation->IsLoop()) ? 0.0 : 0.9 + (float)actualAnimation->FrameAmount()-1;
    }

    (*graph) = RF_AssetManager::Get<RF_Gfx2D>(actualAnimation->Package(), actualAnimation->Basename() + "_" + std::to_string((int)actualFrame));
  }
}

void Animator::setSpeed(float speed)
{
  assert(actualAnimation != nullptr);
  actualAnimation->Speed() = speed;
}

float Animator::getSpeed()
{
  assert(actualAnimation != nullptr);
  return actualAnimation->Speed();
}

bool Animator::hasEndAnimation()
{
  return !animating;
}
