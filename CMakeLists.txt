cmake_minimum_required(VERSION 2.8.0)
set(CMAKE_CXX_FLAGS "-O3 -std=c++17 -Wall -Werror")
project("BatterEngine")
add_library(
  BatterEngine
  SHARED
  src/actor.cpp
  src/animation.cpp
  src/animator.cpp
  src/camera.cpp
  src/inputmanager.cpp
  src/scene.cpp
  src/scenemanager.cpp
  src/sprite.cpp
  src/interfaces/statemachine.cpp
  src/utils/locale.cpp
)

target_include_directories(BatterEngine PUBLIC ./include/)
find_library(ROSQUI_LIB Rosquillera)
find_library(SDL2_LIBRARY SDL2)
target_link_libraries(
  BatterEngine
  PUBLIC
  ${SDL2_LIBRARY}
  SDL2main
  SDL2_image
  SDL2_mixer
  SDL2_ttf
  ${ROSQUI_LIB}
)
