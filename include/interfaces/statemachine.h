/*
  DOUGH ENGINE
  Copyright (C) 2020 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#include <vector>
#include <string>
#include <map>
#include <functional>

struct State
{
    std::string id;
    std::function<void()> action;
    std::string next;

    State():id(""){}
    State(std::string _id, std::function<void()> func, std::string n = ""):id(_id), action(func), next(n){}
};

class StateMachine
{
  protected:
    void addState(std::string id, std::function<void()> func, std::string next = "");
    void doAction();
    void setState(std::string state);
    std::string getState();

  private:
    std::map<std::string, State> stateList;
    std::string activeState = "";
};

#endif //STATEMACHINE_H
