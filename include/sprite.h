/*
  DOUGH ENGINE
  Copyright (C) 2020 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef SPRITE_H
#define SPRITE_H

#include "animator.h"

#include <RosquilleraReforged/rf_process.h>

class Sprite : public RF_Process
{
  public:
    Sprite(std::string name = "sprite");
    virtual ~Sprite();

    virtual void Draw();
    virtual void OnSpriteDraw(){}
    virtual void LateDraw();
    virtual void OnSpriteLateDraw(){}

    template<typename X, typename Y>
    void SetPosition(X x, Y y)
    {
      SetPosition((float)x, (float)y);
    }

    template<typename T>
    void SetPosition(RF_Structs::Vector2<T> pos)
    {
      SetPosition(pos.x, pos.y);
    }

    void SetPosition(float x, float y);

    template<typename X, typename Y>
    void Move(X x, Y y)
    {
      Move((float)x, (float)y);
    }

    template<typename T>
    void Move(RF_Structs::Vector2<T> pos)
    {
      Move(pos.x, pos.y);
    }

    void Move(float x, float y);

    bool updateDepth = true;
    RF_Structs::Vector2<float> realPosition;
    RF_Structs::Vector2<float> offset;

    bool canExitRoom = false;

  protected:
    Animator* animator;
};

#endif //SPRITE_H
