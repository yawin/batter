/*
  DOUGH ENGINE
  Copyright (C) 2020 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef INPUTMANAGER_H
#define INPUTMANAGER_H

#include <string>
#include <map>
#include <vector>

class KeyGroup
{
  public:
    KeyGroup(std::vector<bool*> _keys);
    ~KeyGroup();

    bool Get();

    static KeyGroup* Make(std::vector<bool*> keys);

  private:
      std::vector<bool*> keys;
};

class InputManager
{
  public:
    static void setAxis(std::string name, KeyGroup* positive, KeyGroup* negative);
    static int getAxis(std::string name);
    static void clearAxis(std::string name = "");

    static bool False;

  private:
    static std::map<std::string, std::pair<KeyGroup*, KeyGroup*>> Axis;
};

#endif //INPUTMANAGER_H
