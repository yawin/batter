/*
  DOUGH ENGINE
  Copyright (C) 2020 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef SCENEMANAGER_H
#define SCENEMANAGER_H

#include "scene.h"

#include <RosquilleraReforged/rf_engine.h>
#include <unordered_map>
#include <stack>

class SceneManager
{
  public:
		static SceneManager* instance;

    SceneManager();
    virtual ~SceneManager();

    template <typename S>
    static void RegisterScene(std::string sid = "")
    {
      static_assert(std::is_base_of<Scene, S>::value, "[SceneManager::newTask] S must derive from Scene");
      if(SceneManager::instance == nullptr){SceneManager::instance = new SceneManager();}

      Scene* s = RF_Engine::getTask<S>(RF_Engine::newTask<S>());
      s->sceneid = (sid != "") ? sid : s->type;
      SceneManager::instance->sceneList[s->sceneid] = s;
    }

    template <typename S = Scene>
    static S* Get(std::string key)
    {
      static_assert(std::is_base_of<Scene, S>::value, "[SceneManager::newTask] S must derive from Scene");
      if(instance == nullptr){instance = new SceneManager();}
      return SceneManager::instance->sceneList[key];
    }

    static bool isInList(std::string key);
    static void Start(std::string key);
    static void Stop();
    static void Change(std::string key);
    static std::string activeSceneId();

  private:
    std::unordered_map<std::string, Scene*> sceneList;
    std::stack<Scene*> sceneStack;
};

#endif //SCENEMANAGER_H
