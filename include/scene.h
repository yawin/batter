/*
  DOUGH ENGINE
  Copyright (C) 2020 Yawin <tuzmakel@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef SCENE_H
#define SCENE_H

#include <RosquilleraReforged/rf_engine.h>
#include <RosquilleraReforged/rf_process.h>

#include <vector>

class Scene : public RF_Process
{
  public:
    Scene(std::string sceneName):RF_Process(sceneName){}
    virtual ~Scene();

    virtual void Update();
    virtual void SceneUpdate(){}

    template <typename T>
    T* AddElement()
    {
      static_assert(std::is_base_of<RF_Process, T>::value, "T must derive from RF_Process");
      std::string ret = RF_Engine::newTask<T>(id);
      content.push_back(ret);
      return RF_Engine::getTask<T>(ret);
    }

    void Init();
    void Stop();
    virtual void Configure(){}
    virtual void Exit(){}

    std::string sceneid;

  private:
    std::vector<std::string> content;
};

#endif //SCENE_H
